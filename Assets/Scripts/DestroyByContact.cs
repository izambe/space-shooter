﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;

    
    void Start()
    {
        
        GameObject gmaeControllerObject = GameObject.FindGameObjectWithTag("GameController");
          if (gmaeControllerObject != null)
        {
            gameController = gmaeControllerObject.GetComponent<GameController>();
        }
         if (gameController == null)
        {
            Debug.Log("Cannot find gamecontroller script");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundry")
        {
            return; 
        }
        Instantiate(explosion, transform.position, transform.rotation);
        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
            
        }
        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);

    }
}